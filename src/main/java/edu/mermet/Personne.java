package edu.mermet;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

class Personne {
    private StringProperty nom;
    private IntegerProperty age;

    public Personne(String leNom, int lAge) {
	nom = new SimpleStringProperty(this, "nom", leNom);
	age = new SimpleIntegerProperty(this, "age", lAge);
    }

    @Override
    public String toString() {
	return nom.getValue() + "(" + age.getValue() + ")";
    }

    public StringProperty nomProperty() {
	return nom;
    }

    public IntegerProperty ageProperty() {
	return age;
    }

    public void setNom(String leNom) {
	nom.setValue(leNom);
    }

    public void setAge(int lAge) {
	age.setValue(lAge);
    }
}
