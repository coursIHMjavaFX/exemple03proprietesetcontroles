package edu.mermet;

import javafx.application.Application;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) {
	var ligne = new HBox(20);
	// partie gauche
	var personneGauche = new VBox(5);
	var personneDroite = new VBox(5);

	var panneauNomGauche = new HBox(5);
	var labelNomGauche = new Label("Nom :");
	var champNomGauche = new TextField();
	champNomGauche.setPrefColumnCount(20);
	panneauNomGauche.getChildren().addAll(labelNomGauche, champNomGauche);

	var panneauAgeGauche = new HBox(5);
	var labelAgeGauche = new Label("Âge :");
	var champAgeGauche = new Spinner<>(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 120, 50));
	panneauAgeGauche.getChildren().addAll(labelAgeGauche, champAgeGauche);

	personneGauche.getChildren().addAll(panneauNomGauche, panneauAgeGauche);

	// partie droite
	var panneauNomDroite = new HBox(5);
	var labelNomDroite = new Label("Nom :");
	var champNomDroite = new TextField();
	champNomDroite.setEditable(false);
	champNomDroite.setPrefColumnCount(20);
	panneauNomDroite.getChildren().addAll(labelNomDroite, champNomDroite);

	var panneauAgeDroite = new HBox(5);
	var labelAgeDroite = new Label("Âge :");
	var champAgeDroite = new Spinner<>(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 120, 50));
	panneauAgeDroite.getChildren().addAll(labelAgeDroite, champAgeDroite);

	personneDroite.getChildren().addAll(panneauNomDroite, panneauAgeDroite);


	ligne.getChildren().addAll(personneGauche, personneDroite);
	
        var scene = new Scene(ligne, 800, 200);
        stage.setScene(scene);
        stage.show();

	Personne p1 = new Personne("Jay", 20);
	StringProperty propNom = p1.nomProperty();
	propNom.addListener((prop, ancien, nouveau) -> System.out.println("Le nom est maintenant " + nouveau));
	propNom.bindBidirectional(champNomGauche.textProperty());
	/*Thread t = new Thread(() -> {
			try {
				Thread.sleep(1000);
			} catch(InterruptedException ie) {}
			p1.nomProperty().setValue("Martin");
			try {
				Thread.sleep(1000);
			} catch(InterruptedException ie) {}
			p1.nomProperty().setValue("Durand");
			try {
				Thread.sleep(1000);
			} catch(InterruptedException ie) {}
			p1.nomProperty().setValue("Dupont");
		});
	t.start();
	}*/
	champNomDroite.textProperty().bind(propNom);
	
	//propNom.bindBidirectional(champNomDroite.textProperty());
	IntegerProperty propAge = p1.ageProperty();
	propAge.addListener((prop, ancien, nouveau) -> System.out.println("nouvel age : " + nouveau));
	
	propAge.bindBidirectional(IntegerProperty.integerProperty(champAgeGauche.getValueFactory().valueProperty()));
	propAge.bindBidirectional(IntegerProperty.integerProperty(champAgeDroite.getValueFactory().valueProperty()));
	
    }

    public static void main(String[] args) {
        launch();
    }

}
